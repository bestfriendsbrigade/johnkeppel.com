#!/usr/bin/env bash
gulp --target=production
echo "Deploying to AWS"
aws s3 sync ./dist s3://johnkeppel.com
