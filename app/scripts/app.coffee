'use strict'

###*
 # @ngdoc overview
 # @name pecofolio
 # @description
 # # pecofolio
 #
 # Main module of the application.
###
angular
  .module('pecofolio', [
    'ngAnimate'
    'ngResource'
    'ngTouch'
    'ui.router'
    'youtube-embed'
  ])
  .config(['$locationProvider', ($locationProvider) ->
    $locationProvider.html5Mode true
  ])
  .run(['$rootScope', ($rootScope) ->
    $rootScope.toStateParamIs = (key, value) ->
      return false unless $rootScope.stateTransition.toParams?

      String($rootScope.stateTransition.toParams[key]) is String(value)

    $rootScope.toStateIs = (name, params) ->
      return false unless $rootScope.stateTransition.toState?

      $rootScope.stateTransition.toState.name is name and
        angular.equals(params, $rootScope.stateTransition.toParams)

    $rootScope.toCategoryIs = (slug) ->
      $rootScope.toStateIs 'projects.category', categorySlug: slug

    $rootScope.getNavItemIconClasses = (transitionInProgress) ->
      {
        'fa-plus': !transitionInProgress
        'fa-circle-o-notch': transitionInProgress
        'fa-spin': transitionInProgress
      }

    $rootScope.stateTransition =
      toState: null
      toParams: null
      fromState: null
      fromParams: null

    resetStateTransition = ->
      $rootScope.stateTransition.toState = null
      $rootScope.stateTransition.toParams = null
      $rootScope.stateTransition.fromState = null
      $rootScope.stateTransition.fromParams = null

    $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
      $rootScope.stateTransition.toState = toState
      $rootScope.stateTransition.toParams = toParams
      $rootScope.stateTransition.fromState = fromState
      $rootScope.stateTransition.fromParams = fromParams

    $rootScope.$on '$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
      resetStateTransition()

      states = toState.name.split '.'
      $rootScope.bodyClasses = []
        .concat (states)
        .concat (for index, state of states then do (index, state) -> states[0..(index)].join('-'))

    $rootScope.$on '$stateChangeError', (event, toState, toParams, fromState, fromParams, error) ->
      resetStateTransition()

      console.error error.stack

    $rootScope.$on '$stateNotFound', ->
      resetStateTransition()
  ])
