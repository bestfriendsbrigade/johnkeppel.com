'use strict'

###*
 # @ngdoc function
 # @name pecofolio.controller:Projects
 # @description
 # # Projects
 # Projects controller of the pecofolio.
###
angular.module('pecofolio')
  .controller 'Projects', [
    '$scope', '$stateParams', '$timeout', 'projectCollection',
    ($scope, $stateParams, $timeout, projectCollection) ->
      $scope.projectCollection = projectCollection

      $scope.targetStateSlug = null

      $scope.$on '$stateChangeStart', (event, toState, toParams) -> $scope.targetStateSlug = toParams.projectSlug
      $scope.$on '$stateChangeComplete', (event, toState, toParams) -> $scope.targetStateSlug = null
      $scope.$on '$stateChangeError', (event, toState, toParams) -> $scope.targetStateSlug = null

      $scope.categoryFilter = (project, index) ->
        project.categories.map((category) -> category.slug).indexOf($stateParams.categorySlug) != -1
  ]
