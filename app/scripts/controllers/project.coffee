'use strict'

###*
 # @ngdoc function
 # @name pecofolio.controller:Project
 # @description
 # # Project
 # Project controller of the pecofolio.
###
angular.module('pecofolio')
  .controller 'Project', [
    '$scope', '$stateParams', '$timeout', 'project',
    ($scope, $stateParams, $timeout, project) ->
      assets = []

      $scope.project = project

      $scope.hideMetadata = false

      $timeout (-> $scope.hideMetadata = true), 3000

      Object.defineProperty $scope, 'assets', get: ->
        assets.splice 0, assets.length, project.assets.slice()...

        featuredAsset = assets.filter((asset) -> asset.id is project.featured_asset_id)[0]
        assets.splice assets.indexOf(featuredAsset), 1
        assets.unshift featuredAsset

        assets
  ]
