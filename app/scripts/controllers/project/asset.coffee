'use strict'

###*
 # @ngdoc function
 # @name pecofolio.controller:ProjectAsset
 # @description
 # # ProjectAsset
 # Project asset controller of the pecofolio.
###
angular.module('pecofolio')
  .controller 'ProjectAsset', [
    '$scope', '$stateParams', 'asset', 'assetImage', 'assetTiles',
    ($scope, $stateParams, asset, assetImage, assetTiles) ->
      $scope.asset = asset
      $scope.assetWidth = assetImage.width
      $scope.assetHeight = assetImage.height
      $scope.zoomable = assetImage.width > 875 or assetImage.height > 525
      # $scope.backgroundSize = if assetImage.width < 875 and assetImage.height < 525
      #   'auto'
      # else
      #   'contain'

      $scope.imageTiles = assetTiles
  ]
