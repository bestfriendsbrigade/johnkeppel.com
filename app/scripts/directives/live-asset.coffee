angular.module('pecofolio')
  .directive 'liveAsset', ->
    restrict: 'C'
    scope:
      zoomable: '=liveAssetZoomable'
      dimensions: '=liveAssetDimensions'
    link: (scope, element, attrs) ->
      originalValues = {}

      containerSelector = attrs.liveAssetContainer
      container = element.closest(attrs.liveAssetContainer)
      emitter = if container.length is 0 then element else container

      _zoomIsEnabled = false

      if scope.dimensions?
        assetWidth = scope.dimensions.width
        assetHeight = scope.dimensions.height

      element.addClass('zoomable') if scope.zoomable
      element.css 'background-size', if scope.zoomable then 'contain' else 'auto'

      zoomIsEnabled = -> scope.zoomable and _zoomIsEnabled

      mouseMoveHandler = (e) ->
        containerWidth = element.width()
        containerHeight = element.height()

        mouseX = e.pageX - emitter.offset().left
        mouseY = e.pageY - emitter.offset().top

        xPercent = if assetWidth > containerWidth then (mouseX / containerWidth) * 100 else '50%'
        yPercent = if assetHeight > containerHeight then (mouseY / containerHeight) * 100 else '50%'

        element.css 'background-position', "#{xPercent}% #{yPercent}%"

      startZoom = ->
        originalValues =
          'background-size': element.css 'background-size'
          'background-position': element.css 'background-position'

        return unless zoomIsEnabled()

        element.css 'background-size', 'auto'

        emitter.on 'mousemove', mouseMoveHandler

      endZoom = ->
        element.css 'background-size', originalValues['background-size']
        element.css 'background-position', originalValues['background-position']

        emitter.off 'mousemove', mouseMoveHandler

      element.on 'mouseup', (e) ->
        return unless scope.zoomable
        if (_zoomIsEnabled = !_zoomIsEnabled) is true
          element.addClass 'zoomed'
          startZoom()
          mouseMoveHandler(e)
        else
          element.removeClass 'zoomed'
          endZoom()
