'use strict'

FULL_SIZE_ASSET_SIZE_MODE = 'fit'

COLUMN_COUNT = 5
ROW_COUNT = 3
TILE_SIDE_LENGTH = 175

GRID_WIDTH = COLUMN_COUNT * TILE_SIDE_LENGTH
GRID_HEIGHT = ROW_COUNT * TILE_SIDE_LENGTH

angular.module('pecofolio')
  .config([
    '$urlRouterProvider', '$stateProvider',
    ($urlRouterProvider, $stateProvider) ->
      $urlRouterProvider.when '/', '/all'

      $stateProvider
        .state('projects', {
          abstract:true
          templateUrl:'/views/projects.html'
        })
        .state('projects.category', {
          url: '/:categorySlug'
          views:
            '@projects':
              templateUrl:'/views/project-category.html'
              controller:'Projects'
          resolve:
            projectCollection: [
              'Project', '$stateParams',
              (Project, $stateParams) ->
                slug = if $stateParams.categorySlug is 'all' then null else $stateParams.categorySlug
                Project.query({category: slug}).$promise
            ]
        })
        .state('projects.category.details', {
          url: '/:projectSlug'
          resolve:
            project: [
              '$q', '$stateParams', 'Project',
              ($q, $stateParams, Project) ->
                Project.get({id: $stateParams.projectSlug}).$promise
                # $q.when (project for project in projectCollection.resources when project.slug is $stateParams.projectSlug)?[0]
            ]
          views:
            '@projects':
              templateUrl:'/views/project.html'
              controller:'Project'
        })
        .state("projects.category.details.asset", {
          url:'/:assetId'
          views:
            '@projects.category.details':
              templateUrl:'/views/project/selected-asset.html'
              controller:'ProjectAsset'
          resolve:
            asset: [
              '$stateParams', 'project',
              ($stateParams, project) ->
                (asset for asset in project.assets when asset.id is parseInt($stateParams.assetId))?[0]
            ],
            assetImage: [
              'asset', '$q', '$cacheFactory',
              (asset, $q, $cacheFactory) ->
                deferred = $q.defer()
                promise = deferred.promise

                imageCache = $cacheFactory.get('imageCache') or $cacheFactory('imageCache')
                cachedAssetImage = imageCache.get asset._embedded['peco:asset/attachment'].original_url

                if cachedAssetImage?
                  deferred.resolve cachedAssetImage
                else
                  assetImage = new Image()
                  assetImage.crossOrigin = 'anonymous'
                  assetImage.addEventListener 'load', (e) ->
                    imageCache.put asset._embedded['peco:asset/attachment'].original_url, assetImage
                    deferred.resolve assetImage

                  assetImage.src = asset._embedded['peco:asset/attachment'].original_url

                return promise
            ]
            assetTiles: [
              '$q', 'assetImage',
              ($q, image) ->
                deferred = $q.defer()
                promise = deferred.promise

                tileCanvas = angular.element("<canvas width=\"#{TILE_SIDE_LENGTH}\" height=\"#{TILE_SIDE_LENGTH}\">")[0]
                tileContext = tileCanvas.getContext '2d'

                gridCanvas = angular.element("<canvas width=\"#{GRID_WIDTH}\" height=\"#{GRID_HEIGHT}\">")[0]
                gridContext = gridCanvas.getContext '2d'

                gridContext.fillStyle = 'rgb(50,50,50)'
                gridContext.fillRect 0, 0, GRID_WIDTH, GRID_HEIGHT

                gridRatio = Math.max Math.max(image.width / GRID_WIDTH, image.height / GRID_HEIGHT), 1

                destWidth = image.width / gridRatio
                destHeight = image.height / gridRatio
                destX = Math.ceil (GRID_WIDTH - destWidth) / 2
                destY = Math.ceil (GRID_HEIGHT - destHeight) / 2

                gridContext.drawImage image,
                  0, 0,
                  image.width, image.height,
                  destX, destY,
                  destWidth, destHeight

                gridImage = new Image()
                gridImage.addEventListener 'load', (e) ->
                  tiles = for index in [0...(COLUMN_COUNT * ROW_COUNT)]
                    do (index) ->
                      column = index % COLUMN_COUNT
                      row = Math.floor(index / COLUMN_COUNT)

                      tileContext.drawImage gridImage,
                        column * TILE_SIDE_LENGTH, row * TILE_SIDE_LENGTH,
                        TILE_SIDE_LENGTH, TILE_SIDE_LENGTH,
                        0, 0,
                        TILE_SIDE_LENGTH, TILE_SIDE_LENGTH

                      {url:tileCanvas.toDataURL()}

                  deferred.resolve tiles

                gridImage.src = gridCanvas.toDataURL()

                return promise
            ]
        })
  ])
