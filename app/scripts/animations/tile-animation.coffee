'use strict'

angular.module('pecofolio')
  .animation '.tile-animation', [
    '$timeout', '$animate', '$q',
    ($timeout, $animate, $q) ->
      # @TODO: Don't hardcode enter transition delay, pull instead from CSS property(?)
      {
        enter: (element, done) ->
          tileElements = element.find 'ul.tile-grid li.tile-container .tile'

          transitionPromises = for tileElement, index in tileElements
            do (tileElement, index) ->
              deferred = $q.defer()

              delayMultiplier = Math.floor(index / 5) + (index % 5)

              $timeout (->
                deferred.resolve $animate.addClass(tileElement, 'enter')
              ), (100 * delayMultiplier) + 250

              deferred.promise

          $q.all(transitionPromises)
            .then -> done()

          # (cancelled) ->
          #   console.log "enter animation #{if cancelled then 'was' else 'wasn\'t'} cancelled"

          return

        leave: (element, done) ->
          tileElements = element.find 'ul.tile-grid li.tile-container .tile'

          transitionPromises = for tileElement, index in tileElements
            do (tileElement, index) ->
              deferred = $q.defer()

              delayMultiplier = Math.floor(index / 5) + (index % 5)

              $timeout (->
                deferred.resolve $animate.addClass(tileElement, 'leave')
              ), 100 * delayMultiplier

              deferred.promise

          $q.all(transitionPromises)
            .then -> done()

          # (cancelled) ->
          #   console.log "leave animation #{if cancelled then 'was' else 'wasn\'t'} cancelled"

          return
      }
  ]
