'use strict'

angular.module 'pecofolio'
  .service 'ResourceCollection', [
    '$injector',
    ($injector) ->
      class ResourceCollection
        constructor: (collection) ->
          ResourceClass = collection.constructor

          if ResourceClass.resourceKey?
            resources = collection._embedded?[ResourceClass.resourceKey]
            @resourceKey = ResourceClass.resourceKey

            resources.splice(0, resources.length, resources.map((resource) ->
              new ResourceClass(resource)
            )...)

          angular.extend @, collection

        Object.defineProperty @::, 'resources', get: -> @_embedded[@resourceKey]
  ]