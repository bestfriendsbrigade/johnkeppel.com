'use strict'

angular.module 'pecofolio'
  .service 'Project', [
    '$resource', '$injector',
    ($resource, $injector) ->
      Project = $resource '/projects/:id', {}, {}, {
        resourceKey: 'peco:projects'
      }

      Object.defineProperty Project::, 'assets', get: ->
        return unless (_assets = @_embedded?['peco:project/assets'])?

        _assets.splice(0, _assets.length, _assets.map((asset) ->
          $injector.invoke(['Asset', (Asset) ->
            if asset instanceof Asset then asset else new Asset(asset)
          ])
        )...)

        _assets

      Object.defineProperty Project::, 'categories', get: ->
        return unless (_categories = @_embedded?['peco:project/categories'])?

        _categories.splice(0, _categories.length, _categories.map((category) ->
          $injector.invoke(['Category', (Category) ->
            if category instanceof Category then category else new Category(category)
          ])
        )...)

        _categories

      Object.defineProperty Project::, 'featuredAsset', get: ->
        return unless (_featured_asset = @_embedded?['peco:project/featured-asset'])?

        @_embedded?['peco:project/featured-asset'] = $injector.invoke(['Asset', (Asset) ->
          if _featured_asset instanceof Asset then _featured_asset else new Asset(_featured_asset)
        ])

      Project
  ]