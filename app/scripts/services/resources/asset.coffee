'use strict'

angular.module 'pecofolio'
  .service 'Asset', [
    '$resource', '$injector',
    ($resource, $injector) ->
      Asset = $resource '/assets/:id', {}, {}, {
        resourceKey: 'peco:assets'
      }

      Asset
  ]