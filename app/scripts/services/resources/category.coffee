'use strict'

angular.module 'pecofolio'
  .service 'Category', [
    '$resource', '$injector',
    ($resource, $injector) ->
      Category = $resource '/categories/:id', {}, {}, {
        resourceKey: 'peco:categories'
      }

      Category
  ]