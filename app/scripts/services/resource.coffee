angular.module 'pecofolio'
  .config [
    '$provide',
    ($provide) ->
      $provide.decorator '$resource', [
        '$delegate', '$http', 'ResourceCollection', 'API',
        ($delegate, $http, ResourceCollection, API) ->
          (path, params, actions, options) ->
            url = "#{API.protocol}://#{API.host}"
            url += ":#{API.port}" if API.port?
            url += path

            defaultActions =
              get: {method: 'GET'}
              create: {method: 'POST'}
              update: {method: 'PUT'}
              query:
                method: 'GET'
                isArray: false
                interceptor:
                  response: (response) -> new ResourceCollection(response.resource)

              remove: {method: 'DELETE'}
              'delete': {method: 'DELETE'}

            actions = angular.extend {}, defaultActions, actions

            defaultParams = id: '@id'

            params = angular.extend {}, defaultParams, params

            Resource = $delegate(url, params, actions)

            if options.resourceKey?
              Resource.resourceKey = options.resourceKey

            Resource::$save = ->
              @[if @id? then '$update' else '$create'].apply @, arguments

            Resource
      ]
  ]
