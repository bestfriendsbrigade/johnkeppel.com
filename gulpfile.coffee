'use strict'

gulp = require 'gulp'

$ = require('gulp-load-plugins')(
  pattern: ['gulp-*', 'del', 'browser-sync', 'wiredep', 'connect-modrewrite', 'yargs']
)

# Configurable paths for the application
appDir = require('./bower.json').appPath or 'app'
destDir = 'dist'
tempDir = '.tmp'

argv = $.yargs.argv

target = argv.target || 'development'

wiredepConfig =
  exclude: [
    /bootstrap\/dist\/css\/.+/
    # /jquery\/.+/
  ]
  fileTypes:
    html:
      replace:
        js: (filePath) ->
          "<script src=\"vendor/#{(filePath.split('/').pop())}\"></script>"
        css: (filePath) ->
          "<link rel=\"stylesheet\" href=\"vendor/#{(filePath.split('/').pop())}\"></link>"

gulp.task 'banner', (complete) ->
  console.log "Environment: #{target}"
  complete()

gulp.task 'bower:js', (complete) ->
  if (jsFiles = $.wiredep(wiredepConfig).js)?
    gulp.src jsFiles
      .pipe gulp.dest("#{tempDir}/vendor")
  else
    complete()

gulp.task 'bower:css', (complete) ->
  if (cssFiles = $.wiredep(wiredepConfig).css)?
    gulp.src cssFiles
      .pipe gulp.dest("#{tempDir}/vendor")
  else
    complete()

gulp.task 'bower', gulp.parallel('bower:js', 'bower:css')

gulp.task 'browser-sync', ->
  return $.browserSync(
    server:
      baseDir: [destDir, tempDir]
      middleware: [
        $.connectModrewrite(['^[^\\.]*$ /index.html [L]'])
      ]
    port: 9000
  )

gulp.task 'clean', (complete) -> $.del "#{destDir}/*", complete

gulp.task 'config', ->
  gulp.src "config/#{target}.coffee"
    .pipe $.coffee({}).on('error', $.util.log)
    .pipe $.rename('config.js')
    .pipe gulp.dest(tempDir + '/scripts')

# gulp.task 'icons', ->
#   gulp.src 'bower_components/material-design-icons/sprites/svg-sprite/*.svg'
#     .pipe gulp.dest "#{destDir}/icons"

gulp.task 'images', ->
  gulp.src "#{appDir}/**/*.{png,jpg,gif,ico}"
    .pipe gulp.dest("#{destDir}")

gulp.task 'fonts', ->
  gulp.src "#{appDir}/fonts/**/*"
    .pipe gulp.dest("#{destDir}/fonts")

gulp.task 'copy-resume', ->
  gulp.src "#{appDir}/resume.pdf"
    .pipe gulp.dest(destDir)

gulp.task 'font-awesome', ->
  gulp.src "bower_components/components-font-awesome/fonts/*"
    .pipe gulp.dest("#{destDir}/fonts")

gulp.task 'less', ->
  gulp.src ["#{appDir}/styles/**/*.less", "!#{appDir}/styles/**/_*.less"]
    .pipe $.less(paths: ['./bower_components'])
    .pipe gulp.dest("#{tempDir}/styles")
    .pipe $.browserSync.reload(stream:true)

# gulp.task 'scripts', ->
#   gulp.src "#{appDir}/scripts/**/*.js"
#     .pipe gulp.dest("#{destDir}/scripts")
#     .pipe $.browserSync.reload(stream:true)

gulp.task 'coffee', ->
  gulp.src "#{appDir}/scripts/**/*.coffee"
    .pipe $.coffee({}).on('error', $.util.log)
    .pipe gulp.dest("#{tempDir}/scripts")
    .pipe $.browserSync.reload(stream:true)

gulp.task 'jade', ->
  gulp.src ["#{appDir}/**/*.jade", "!#{appDir}/**/_*.jade"]
    .pipe $.jade({
      pretty: true
    })
    .pipe gulp.dest(destDir)

gulp.task 'apache-config', ->
  gulp.src "#{appDir}/.htaccess"
    .pipe gulp.dest(destDir)

gulp.task 'usemin', ->
  gulp.src "#{destDir}/index.html"
    .pipe($.usemin({
      # // css: [$.minifyCss(), 'concat'],
      css: ['concat', $.rev()],
      # // html: [
      # //   $.minifyHtml({
      # //     empty: true
      # //   })
      # // ],
      js: ['concat', $.rev()]
    }))
    .pipe gulp.dest(destDir)

gulp.task 'watch', ->
  gulp.watch "#{appDir}/index.jade", gulp.series('jade', 'wiredep')
  gulp.watch "#{appDir}/**/*.jade", gulp.series('jade', 'wiredep')
  gulp.watch "#{appDir}/scripts/**/*.coffee", gulp.series('coffee')
  gulp.watch "#{appDir}/styles/**/*.less", gulp.series('less')

gulp.task 'wiredep', ->
  gulp.src("#{destDir}/index.html")
    .pipe $.wiredep.stream(wiredepConfig)
    .pipe gulp.dest(destDir)
    .pipe $.browserSync.reload(stream:true)

buildTasks = [
  'banner',
  'clean',
  'jade',
  gulp.parallel(
    'coffee',
    'less',
    'images',
    'fonts',
    'font-awesome',
    'config',
    'copy-resume',
    'apache-config'
  ),
  'bower',
  'wiredep'
]
buildTasks.push('usemin') if target is 'production' and !argv.skipMin

gulp.task 'build', gulp.series(buildTasks...)

gulp.task 'serve', gulp.series('build', gulp.parallel('browser-sync', 'watch'))

gulp.task 'default', gulp.series('build')
